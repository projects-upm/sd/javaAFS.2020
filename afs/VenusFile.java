// Clase de cliente que define la interfaz a las aplicaciones.
// Proporciona la misma API que RandomAccessFile.
package afs;

import java.rmi.*;
import java.io.*;

public class VenusFile {

    private File file;
    private RandomAccessFile randomAccessFile;
    private boolean modified;

    private Venus venus;

    public static final String cacheDir = "Cache/";

    public VenusFile(Venus venus, String fileName, String mode)
            throws RemoteException, IOException, FileNotFoundException {
        this.venus = venus;
        this.file = new File(cacheDir + fileName);
        ViceReader viceReader = this.venus.getVice().download(fileName, mode, this.venus.getVenusCB());
        if (!file.exists()) {

            this.randomAccessFile = new RandomAccessFile(this.file, "rw");
            byte buffer[];
            do {
                buffer = viceReader.read(venus.getBlockSize());
                if (buffer != null) {
                    randomAccessFile.write(buffer);
                }
            } while (buffer != null);

            this.randomAccessFile.close();

            this.randomAccessFile = new RandomAccessFile(cacheDir + fileName, mode);
        } else {
            this.randomAccessFile = new RandomAccessFile(cacheDir + fileName, mode);
        }
        viceReader.close();
    }

    public int read(byte[] b) throws RemoteException, IOException {
        return this.randomAccessFile.read(b);
    }

    public void write(byte[] b) throws RemoteException, IOException {
        this.randomAccessFile.write(b);
        this.modified = true;
    }

    public void seek(long p) throws RemoteException, IOException {
        this.randomAccessFile.seek(p);
    }

    public void setLength(long l) throws RemoteException, IOException {
        this.randomAccessFile.setLength(l);
        this.modified = true;
    }

    public void close() throws RemoteException, IOException {
        if (this.modified) {

            ViceWriter viceWriter = this.venus.getVice().upload(this.file.getName(), this.venus.getVenusCB());

            this.randomAccessFile.seek(0);

            byte buffer[];
            long pos = this.randomAccessFile.getFilePointer();
            long len = this.randomAccessFile.length();
            int tam = venus.getBlockSize();

            viceWriter.setLength(len);

            while (pos + tam <= len) {
                buffer = new byte[tam];

                this.randomAccessFile.read(buffer);

                viceWriter.write(buffer);

                pos = this.randomAccessFile.getFilePointer();
            }

            if (len - pos > 0) {
                buffer = new byte[(int) (len - pos)];

                this.randomAccessFile.read(buffer);

                viceWriter.write(buffer);
            }

            viceWriter.close();
        }
        randomAccessFile.close();
    }
}
