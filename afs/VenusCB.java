// Interfaz de cliente que define los métodos remotos para gestionar
// callbacks
package afs;

import java.io.*;
import java.rmi.*;

public interface VenusCB extends Remote {

    /* añada los parámetros que requiera */
    public void invalidate(String fileName) throws RemoteException, IOException;

    /* añada los métodos remotos que requiera */
}
