// Implementación de la interfaz de servidor que define los métodos remotos
// para completar la descarga de un fichero
package afs;

import java.io.*;
import java.rmi.*;
import java.rmi.server.*;

public class ViceReaderImpl extends UnicastRemoteObject implements ViceReader {
    private static final String AFSDir = "AFSDir/";

    private File file;
    private RandomAccessFile randomAccessFile;

    private Vice vice;

    /* añada los parámetros que requiera */
    public ViceReaderImpl(String fileName, String mode, Vice vice) throws FileNotFoundException, RemoteException {
        this.vice = vice;
        this.file = new File(AFSDir + fileName);
        this.randomAccessFile = new RandomAccessFile(this.file, mode);

    }

    public byte[] read(int tam) throws RemoteException, IOException {
        long pos = this.randomAccessFile.getFilePointer();
        long len = this.randomAccessFile.length();

        byte buffer[];

        if (len - pos <= 0) {
            return null;
        }
        if (pos + tam <= len) {
            buffer = new byte[tam];
        } else {
            buffer = new byte[(int) (len - pos)];
        }
        this.randomAccessFile.read(buffer);
        return buffer;
    }

    public void close() throws RemoteException, IOException {
        this.randomAccessFile.close();
        this.vice.bind(this.file.getName()).readLock().unlock();
        this.vice.unbind(this.file.getName());
        // System.out.println("Desbloqueo lectura.");
    }

}
