// Implementación de la interfaz de servidor que define los métodos remotos
// para iniciar la carga y descarga de ficheros
package afs;

import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.*;
import java.rmi.server.*;

public class ViceImpl extends UnicastRemoteObject implements Vice {

    private LockManager lockManager;

    private Map<String, Set<VenusCB>> dowloadedFiles;

    public ViceImpl() throws RemoteException {
        this.lockManager = new LockManager();
        this.dowloadedFiles = new HashMap<>();
    }

    /* añada los parámetros que requiera */
    public ViceReader download(String fileName, String mode, VenusCB venusCB)
            throws RemoteException, FileNotFoundException, IOException {
        this.bind(fileName).readLock().lock();
        // System.out.println("Bloqueo lectura.");
        if (dowloadedFiles.containsKey(fileName)) {
            dowloadedFiles.get(fileName).add(venusCB);
        } else {
            Set<VenusCB> clients = new HashSet<>();
            clients.add(venusCB);
            dowloadedFiles.put(fileName, clients);
        }
        return new ViceReaderImpl(fileName, mode, this);
    }

    /* añada los parámetros que requiera */
    public ViceWriter upload(String fileName, VenusCB venusCB)
            throws RemoteException, FileNotFoundException, IOException {
        this.bind(fileName).writeLock().lock();
        // System.out.println("Bloqueo escritura.");
        if (dowloadedFiles.containsKey(fileName)) {
            for (VenusCB client : dowloadedFiles.get(fileName)) {
                if (!client.equals(venusCB)) {
                    client.invalidate(fileName);
                }
            }
        } else {
            Set<VenusCB> clients = new HashSet<>();
            clients.add(venusCB);
            dowloadedFiles.put(fileName, clients);
        }
        return new ViceWriterImpl(fileName, "rw", this);
    }

    public synchronized ReentrantReadWriteLock bind(String fileName) throws RemoteException {
        return this.lockManager.bind(fileName);
    }

    public synchronized void unbind(String fileName) throws RemoteException {
        this.lockManager.unbind(fileName);
    }

}
