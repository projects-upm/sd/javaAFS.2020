// Clase de cliente que inicia la interacción con el servicio de
// ficheros remotos
package afs;

import java.net.MalformedURLException;
import java.rmi.*;

public class Venus {
    private String host;
    private int port;
    private int blockSize;

    private Vice vice;

    private VenusCB venusCB;

    public Venus() throws NotBoundException, MalformedURLException, RemoteException {
        this.host = System.getenv("REGISTRY_HOST");
        this.port = Integer.parseInt(System.getenv("REGISTRY_PORT"));
        this.blockSize = Integer.parseInt(System.getenv("BLOCKSIZE"));

        this.vice = (Vice) Naming.lookup("//" + host + ":" + port + "/AFS");

        this.venusCB = new VenusCBImpl();
    }

    /**
     * @return the host
     */
    public String getHost() {
        return this.host;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return this.port;
    }

    /**
     * @return the blockSize
     */
    public int getBlockSize() {
        return this.blockSize;
    }

    /**
     * @return the vice
     */
    public Vice getVice() {
        return this.vice;
    }

    /**
     * @return the venusCB
     */
    public VenusCB getVenusCB() {
        return venusCB;
    }
}
