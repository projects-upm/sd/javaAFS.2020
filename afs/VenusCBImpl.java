// Implementación de la interfaz de cliente que define los métodos remotos
// para gestionar callbacks
package afs;

import java.io.File;
import java.io.IOException;
import java.rmi.*;
import java.rmi.server.*;

public class VenusCBImpl extends UnicastRemoteObject implements VenusCB {

    public VenusCBImpl() throws RemoteException {
    }

    /* añada los parámetros que requiera */
    public void invalidate(String fileName) throws RemoteException, IOException {
        File file = new File("Cache/" + fileName);
        file.delete();
    }

}
