// Implementación de la interfaz de servidor que define los métodos remotos
// para completar la carga de un fichero
package afs;

import java.io.*;
import java.rmi.*;
import java.rmi.server.*;

public class ViceWriterImpl extends UnicastRemoteObject implements ViceWriter {

    private static final String AFSDir = "AFSDir/";

    private File file;
    private RandomAccessFile randomAccessFile;
    private Vice vice;

    /* añada los parámetros que requiera */
    public ViceWriterImpl(String fileName, String mode, Vice vice) throws RemoteException, FileNotFoundException {
        this.vice = vice;
        this.file = new File(AFSDir + fileName);
        this.randomAccessFile = new RandomAccessFile(this.file, mode);
    }

    public void write(byte[] b) throws RemoteException, IOException {
        this.randomAccessFile.write(b);
    }

    public void close() throws RemoteException, IOException {
        this.randomAccessFile.close();
        this.vice.bind(this.file.getName()).writeLock().unlock();
        // System.out.println("Desbloqueo escritura.");
        this.vice.unbind(this.file.getName());
    }

    public void setLength(long l) throws RemoteException, IOException {
        this.randomAccessFile.setLength(l);
    }
}
