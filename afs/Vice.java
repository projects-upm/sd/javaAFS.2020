// Interfaz de servidor que define los métodos remotos para iniciar
// la carga y descarga de ficheros
package afs;

import java.io.*;
import java.rmi.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public interface Vice extends Remote {
        /* añada los parámetros que requiera */
        public ViceReader download(String fileName, String mode, VenusCB venusCB)
                        throws RemoteException, FileNotFoundException, IOException;

        /* añada los parámetros que requiera */
        public ViceWriter upload(String fileName, VenusCB venusCB)
                        throws RemoteException, FileNotFoundException, IOException;

        /* añada los métodos remotos que requiera */
        public ReentrantReadWriteLock bind(String fileName) throws RemoteException;

        public void unbind(String fileName) throws RemoteException;
}
